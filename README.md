# lucascampanelli/bookmanagement

---

### Sistema para administração de livros.

## 📝 Conteúdo

- [Sobre](#about)
- [Iniciando](#getting_started)

## 🧐 Sobre

Sistema para administração de livros de uma loja.

## 🏁 Iniciando

<ul>
<li>Baixe o projeto (bookmanagement)</li>
<li>Coloque na pasta do servidor PHP</li>
<li>Importe o banco de dados <i>bookshop.sql</i> para o servidor PHP</li>
<li>Aproveite!</li>
</ul>
