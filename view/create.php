<?php
    //Requirindo arquivo de configuração
    require '../config.php';
    //
    
    //Conexão do BD
    include '../model/connection.php';
    $conn = conexao();
    //
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BookManagement</title>
    <link rel='stylesheet' href=style/style.css>
    <link rel="icon" href='assets/bookmanagement.ico'>
</head>
<body>
    <h1 class='title'>Adicionar livro</h1>
    <center><h1 class='error' id='ajaxResponse'></h1></center>
    <div class='conteiner'>
    <form name='addForm' id='create' method='POST' action='javascript:create();'>
        <label>Nome</label>
        <input type='text' name='nome'>
        <label>Autor</label>
        <input type='text' name='autor'>
        <label>Quantidade de páginas</label>
        <input type='number' min=0 name='qtd'>
        <label>Preco</label>
        <input name='preco' id='preco' data-affixes-stay="true" data-thousands="." data-decimal=",">
        <label>Slug</label>
        <input type='text' name='slug'>
        <div class='buttons'><button type='submit' class='add'>Salvar</button><button type='reset' class='voltar' onclick='window.location="index.php"'>Voltar</button></div>
    </form>
    <?php
        if(isset($_REQUEST["validar"]) && $_REQUEST["validar"] == true):
            echo $_POST["nome"];
        endif;
    ?>
    </conteiner>
</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="../script/jquery.maskMoney.min.js"></script>
<script>

    $(document).ready(function(){
            $("#preco").maskMoney(); //Máscara monetária para campo do preço
    });
    
    function create(){
        data = $("#create").serialize(); //Dados que serão enviados no corpo da requisição

        $.ajax({
            type: 'POST', //Tipo de requisição
            url: '../model/addbook.php', //Onde a requisição ocorrerá
            data: data, //Dados que serão enviados
            dataType: 'json' //Tipo de resposta
        }).done(function(response){
            if(response.status){
                location.href = "index.php";
            }
            else{
                document.getElementById('ajaxResponse').innerHTML = response.msg;
            }
        }).fail(function(xhr, desc, err){
            document.getElementById('ajaxResponse').innerHTML = "Falha ao criar produto. Tente novamente.";
            console.log(xhr);
            console.log("detalhes: "+desc+" Erro: "+ err);
        });
    }

</script>