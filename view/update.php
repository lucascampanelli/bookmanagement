<?php
    //Requirindo arquivo de configuração
    require '../config.php';
    //

    //Conexão do BD
    include '../model/connection.php';
    $conn = conexao();
    //
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BookManagement</title>
    <link rel='stylesheet' href=style/style.css>
    <link rel="icon" href='assets/bookmanagement.ico'>
</head>
<body>
    <h1 class='title'>Editar livro</h1>
    <center><h1 class='error' id='ajaxResponse'></h1></center>
    <div class='conteiner'>
    <?php
        //Query para receber as informações do livro para editar
        $select = "SELECT * FROM book WHERE id = {$_GET['id']}";
        $res = $conn->prepare($select);
        $res->execute();
        
        $result = $res->fetchAll();
        //

        foreach($result as $book):
            //Exibindo em um form todas as informações
            ?>
                <form id='create' name='update' method='POST' action='javascript:create();'>
                    <input type='hidden' name='id' value='<?= $book["id"] ?>'>
                    <input type='hidden' name='currentName' value='<?= $book["nome"] ?>'>
                    <label>Nome</label>
                    <input type='text' name='nome' value='<?= $book["nome"] ?>'>
                    <label>Autor</label>
                    <input type='text' name='autor' value='<?= $book["autor"] ?>'>
                    <label>Quantidade de páginas</label>
                    <input type='number' name='qtd' min=0 value='<?= $book["qtd_pag"] ?>'>
                    <label>Preco</label>
                    <input name='preco' id='preco' data-affixes-stay="true" data-thousands="." data-decimal="," value='<?= $book['preco'] ?>'>
                    <label class='statuslbl'>Status</label>
                    
                    <?php
                        if($book["flag"] == 1):
                            ?>
                                <label for='flag'>Ativo</label>
                                <input class='radio' name='flag' type='radio' value=1 checked>
                                <label for='flag'>Inativo</label>
                                <input class='radio' name='flag' type='radio' value=0>
                            <?php
                        else:
                            ?>
                                <label for='flag'>Ativado</label>
                                <input class='radio' name='flag' type='radio' value=1>
                                <label for='flag'>Desativado</label>
                                <input class='radio' name='flag' type='radio' value=0 checked>
                            <?php
                        endif;
                    ?>
                    <label>Slug</label>
                    <input type='text' name='slug' value='<?= $book["slug"] ?>'>
                    <div class='buttons'><button type='submit' class='add'>Salvar</button><button type='reset' class='voltar' onclick='window.location="index.php"'>Voltar</button></div>
                </form>
            <?php
        endforeach;
        //
    ?>
    </div>
</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="../script/jquery.maskMoney.min.js"></script>
<script>

    $(document).ready(function(){
            $("#preco").maskMoney(); //Máscara monetária para campo de preço
    });

    function create(){
        data = $("#create").serialize(); //Definindo todos os dados do formulário na variável

        $.ajax({
            type: 'POST', //Tipo da requisição (POST)
            url: '../model/updatebook.php', //Endereço da chamada
            data: data, //Dados que serão enviados
            dataType: 'json' //Tipo de dados da resposta (JSON)
        }).done(function(response){
            if(response.status){
                location.href = "index.php"; //Se der certo, envia para a página de listagem
            }
            else{
                document.getElementById('ajaxResponse').innerHTML = response.msg; //Caso contrário, mostra o erro da requisição
            }
        }).fail(function(xhr, desc, err){
            document.getElementById('ajaxResponse').innerHTML = "Falha ao criar produto. Tente novamente.";
            console.log(xhr);
            console.log("detalhes: "+desc+" Erro: "+ err);
        });
    }

</script>