<?php
    //Requirindo arquivo de configuração
    require '../config.php';
    //

    //Conexão do BD
    include '../model/connection.php';
    $conn = conexao();
    //
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BookManagement</title>
    <link rel='stylesheet' href=style/style.css>
    <link rel="icon" href='assets/bookmanagement.ico'>
</head>
<body>
    <h1 class='title'>Livros</h1>
    <div class='conteiner'>
    <table>
        <tr>
            <th class='id'>ID</th>
            <th>Nome</th>
            <th>Autor</th>
            <th>Quantidade de Páginas</th>
            <th>Preço</th>
            <th>Status</th>
            <th>Última modificação</th>
            <th>Slug</th>
            <th>Ações</th>
        </tr>
            <?php
                //Query para leitura de todos os livros
                $select = "SELECT * FROM BOOK";
                $res = $conn->prepare($select);
                $res->execute();

                $result = $res->fetchAll();
                //

                //Exibindo todos os livros
                foreach($result as $book):
                    ?>
                    <tr>
                        <td><?= $book['id'] ?></td>
                        <td><?= $book['nome'] ?></td>
                        <td><?= $book['autor'] ?></td>
                        <td><?= $book['qtd_pag'] ?></td>
                        <td>R$ <?= $book['preco'] ?></td>
                        <td>
                                <?php 
                                    //Exibindo a flag caso seja ativa
                                    if($book['flag'] == 1):
                                        //Mostra um checkbox para desativar
                                        echo "<div>".
                                            "<form class='formStatus' id='changeStatus".$book['id']."' action='".URL."model/changeStatus.php?id=".$book['id']."' method='POST'>".
                                            "<input name='ativo' type='checkbox' class='checkbox' onclick='enviar(".$book['id'].")' checked>"."
                                            <label class='lblStatus'>Desativar</label>"."
                                            </div>"."
                                            </form>";
                                    //Exibindo caso a flag seja inativa
                                    else: 
                                        //Mostra um checkbox para ativar
                                        echo "<div>"."
                                            <form class='formStatus' id='changeStatus".$book['id']."' action='".URL."model/changeStatus.php?id=".$book['id']."' method='POST'>".
                                            "<input name='inativo' type='checkbox' class='checkbox' onclick='enviar(".$book['id'].")'>".
                                            "<label class='lblStatus'>Ativar</label>".
                                            "</div>".
                                            "</form>"; 
                                    endif;
                                ?>
                        </td>
                        <td><?= $book['last_mod'] ?></td>
                        <td><?= $book['slug'] ?></td>
                        <td class='acoes'><button class='edit' onclick="window.location='update.php?id=<?=$book['id']?>'">Editar</button><button class='remove' onclick="window.location='../model/removebook.php?slug=<?=$book['id']?>'">Remover</button></td>
                    </tr>
                    <?php
                endforeach;
                //
            ?>
    </table>
    </div>
    <button class='add' onclick="window.location='create.php'">Adicionar</button>
</body>
</html>

<script type='text/javascript'>
    function enviar(id){
        document.getElementById('changeStatus'+id).submit(); //Ativando formulário
    }
</script>