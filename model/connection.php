<?php
    function conexao(){
        $banco = "bookshop"; //Nome do banco de dados
        $host = "localhost"; //Host do banco de dados (Localhost)
        $usuario = "root"; //Usuário
        $senha= ""; //Senha
        $conn = new PDO("mysql:host={$host};dbname={$banco}","{$usuario}","{$senha}");
        $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
        return $conn;
    }
