<?php
    //Requirindo arquivo de configuração
    require '../config.php';
    //

    //Conexão do BD
    include 'connection.php';
    $conn = conexao();
    //

    //Query para alterar o status do produto
    $change = "UPDATE book SET flag = ? where id = {$_GET['id']}";

    $res = $conn->prepare($change);

    //Se a opção mandada for para ativar, é atribuído o valor 1, caso contrário, é atribuído o valor 2
    (isset($_POST['inativo']) && $_POST['inativo']) ? $res->bindValue(1, 1) : $res->bindValue(1, 0);

    $res->execute();
    //

    //Se ocorrer algum erro na requisição
    if($res->errorCode() != "00000"){
        $erro = "Erro código " . $res->errorCode() . ": ";
        $erro .= implode(", ", $res->errorInfo());
        echo $erro;
    }
    else{
        //Chama a página de listagem novamente
        echo '<script> location.href = "'.URL.'view/index.php" </script>';
    }