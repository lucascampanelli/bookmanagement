<?php
    //Requirindo arquivo de configuração
    require '../config.php';
    //

    //Conexão do BD
    include 'connection.php';
    $conn = conexao();
    //

    //Variável de resposta para a chamada AJAX
    $resposta = array();
    header('Content-type: application/json'); //Tipo de resposta (JSON)
    
    //Query para validação dos dados do produto
    $valid = "SELECT * FROM book WHERE nome = '".$_POST['nome']."' and nome != '".$_POST['currentName']."'";

    $res = $conn->prepare($valid);
    $res->execute();
    //

    //Se houver mais de um produto com o mesmo nome
    if($res->fetchColumn() > 0){
        $resposta['msg'] = "Já existe um livro com o mesmo nome."; //Define a mensagem de erro
        $resposta['status'] = false; //Define o status de erro para a chamada de AJAX
        echo json_encode($resposta); //Envia um JSON com a resposta
    }
    else if($_POST['qtd'] == 0 || $_POST['qtd'] < 0){
        $resposta['msg'] = "Quantidade de páginas inválida."; //Define a mensagem de erro
        $resposta['status'] = false; //Define o status de erro para a chamada de AJAX
        echo json_encode($resposta); //Envia um JSON com a resposta
    }
    else if($_POST['preco'] == 0 || $_POST['preco'] < 0){
        $resposta['msg'] = "Digite um preço válido (Não pode ser igual a zero)."; //Define a mensagem de erro
        $resposta['status'] = false; //Define o status de erro para a chamada de AJAX
        echo json_encode($resposta); //Envia um JSON com a resposta
    }
    else{
        //Query para atualizar produto
        $update = "UPDATE book SET nome = ?, autor = ?, qtd_pag = ?, flag = ?, last_mod = ?, preco = ?, slug = ? WHERE id = ?";

        $res = $conn->prepare($update);
        $res->bindValue(1, $_POST['nome']);
        $res->bindValue(2, $_POST['autor']);
        $res->bindValue(3, $_POST['qtd']);
        $res->bindValue(4, $_POST['flag']);
        $res->bindValue(5, date('Y/m/d'));
        $res->bindValue(6, $_POST['preco']);
        $res->bindValue(7, $_POST['slug']);
        $res->bindValue(8, $_POST['id']);

        $res->execute();
        //

        //Se houver algum erro na requisição
        if($res->errorCode() != "00000"){
            $erro = "Erro código " . $res->errorCode() . ": ";
            $erro .= implode(", ", $res->errorInfo());
            echo $erro;
            $resposta['status'] = false; //Define o status de erro para a chamada AJAX
            echo json_encode($resposta); //Envia a resposta
        }
        else{
            $resposta['status'] = true; //Define o status de concluído para a chamada AJAX
            echo json_encode($resposta); //Envia a resposta
        }
    }