<?php
    //Requirindo arquivo de configuração
    require '../config.php';
    //

    //Conexão do BD
    include 'connection.php';
    $conn = conexao();
    //

    //Query para remover o produto desejado
    $remove = "DELETE FROM book WHERE id = {$_GET['slug']}";

    $res = $conn->prepare($remove);
    $res->execute();
    //

    //Se ocorrer algum erro na execução da query
    if($res->errorCode() != "00000"){
        $erro = "Erro código " . $res->errorCode() . ": ";
        $erro .= implode(", ", $res->errorInfo());
        echo $erro;
    }
    else{
        echo '<script> location.href = "'.URL.'view/index.php" </script>'; //Retorna para a página de listagem
    }
