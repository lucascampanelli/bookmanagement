<?php
    //Requirindo arquivo de configuração
    require '../config.php';
    //

    //Conexão do BD
    include 'connection.php';
    $conn = conexao();
    //

    //Variável de resposta para a chamda de AJAX
    $resposta = array();
    header('Content-type: application/json'); //Tipo de resposta (JSON)

    //Query para verificar a validade dos dados
    $valid = "SELECT * FROM BOOK WHERE nome = '".$_POST['nome']."'";

    $res = $conn->prepare($valid);
    $res->execute();
    //

    //Se existir mais de um produto com o nome definido
    if($res->fetchColumn() > 0){
        $resposta['msg'] = "Já existe um livro com o mesmo nome."; //Define a mensagem de erro
        $resposta['status'] = false; //Define o status de erro para a chamada de AJAX
        echo json_encode($resposta); //Envia um JSON com a resposta
    }
    //Se a quantidade de páginas for igual ou menor que zero
    else if($_POST['qtd'] == 0 || $_POST['qtd'] < 0){
        $resposta['msg'] = "Quantidade de páginas inválida."; //Define a mensagem de erro
        $resposta['status'] = false; //Define o status de erro para a chamada de AJAX
        echo json_encode($resposta); //Envia um JSON com a resposta
    }
    //Se o preço for igual o menor que zero
    else if($_POST['preco'] == 0 || $_POST['preco'] < 0){
        $resposta['msg'] = "Digite um preço válido (Não pode ser igual a zero)."; //Define a mensagem de erro
        $resposta['status'] = false; //Define o status de erro para a chamada AJAX
        echo json_encode($resposta); //Envia um JSON com a resposta
    }
    else{
        //Query para adicionar o produto
        $sql = "INSERT INTO BOOK (nome, autor, qtd_pag, preco, flag, last_mod, slug, id_loja) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        $res = $conn->prepare($sql);
        $res->bindValue(1, $_POST['nome']);
        $res->bindValue(2, $_POST['autor']);
        $res->bindValue(3, $_POST['qtd']);
        $res->bindValue(4, $_POST['preco']);
        $res->bindValue(5, 0);
        $res->bindValue(6, date('Y/m/d'));
        $res->bindValue(7, $_POST['slug']);
        $res->bindValue(8, 1);

        $res->execute();
        //

        //Se ocorrer algum erro na requisição
        if($res->errorCode() != "00000"){
            $erro = "Erro código " . $res->errorCode() . ": ";
            $erro .= implode(", ", $res->errorInfo());
            echo $erro;
            $resposta['status'] = false; //Define o status de erro para a chamada AJAX
            echo json_encode($resposta); //Envia a resposta
        }
        else{
            $resposta['status'] = true; //Define o status de finalizado para a chamada AJAX
            echo json_encode($resposta); //Envia a resposta
        }
    }
